<?php

namespace Drupal\ditto\Plugin\paragraphs\Behavior;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\PrependCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\EntityStorageException;
use LogicException;

/**
 * Provides a way to define which paragraph has the ditto functionality.
 *
 * @ParagraphsBehavior(
 *   id = "ditto_paragraph",
 *   label = @Translation("Ditto Paragraphs"),
 *   description = @Translation("Enables copy-paste functionality on this paragraph type."),
 *   weight = 0
 * )
 */
class DittoParagraph extends ParagraphsBehaviorBase {

  /**
   * The Entity Repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * ParagraphsLayoutPlugin constructor.
   *
   * @param array $configuration
   *   The configuration array.
   * @param string $plugin_id
   *   This plugin id.
   * @param mixed $plugin_definition
   *   Plugin definition.
   * @param \Drupal\Core\Entity\EntityFieldManager $entity_field_manager
   *   Entity field manager service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityFieldManager $entity_field_manager,
    EntityRepositoryInterface $entity_repository) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_field_manager);
    $this->entityRepository = $entity_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_field.manager'),
      $container->get('entity.repository')
    );
  }

  /**
   * Get this plugins Behavior settings.
   *
   * @return array
   *   Behavior settings.
   */
  private function getSettings(ParagraphInterface $paragraph) {
    $settings = $paragraph->getAllBehaviorSettings();
    return $settings[$this->pluginId] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    if (!$this->configuration['enabled']) {
      return $form;
    }
    $config = $this->getSettings($paragraph);
    $form['paragraph_uuid'] = [
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#required' => TRUE,
      '#title' => $this->t('Unique ID'),
      '#description' => $this->t('Paste the unique id of the paragraph you want to copy.</br>Example <em>7ffd646a-8383-11ec-a8a3-0242ac120002</em>.'),
      '#attributes' => [
        'class' => ['js-ditto-paste-uuid'],
      ],
    ];
    $form['rebuild'] = [
      '#type' => 'submit',
      '#value' => $this->t('Paste paragraph'),
      '#submit' => [[$this, 'pasteSubmit']],
      '#limit_validation_errors' => [],
      '#parent_entity' => $paragraph->getParentEntity(),
      '#attributes' => [
        'class' => ['js-hide'],
      ],
      '#ajax' => [
        'callback' => [$this, 'pasteAjax'],
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   * @throws Exception
   */
  public function validateBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    if (end($trigger['#parents']) === 'submit') {
      $form_state->setError($form, 'Please use the Paste paragraph button instead.');
    }
    $uuid = $form_state->getValue('paragraph_uuid');

    try {
      $source_paragraph = $this->entityRepository->loadEntityByUuid('paragraph', $uuid);
      if (!$source_paragraph) {
        throw new EntityStorageException('Could not find a paragraph with UUID.');
      }
      if ($source_paragraph->isNew()) {
        throw new LogicException('Cannot replicate a new paragraph.');
      }
      $source_paragraph_type = $source_paragraph->bundle();
      $form_state->setTemporaryValue('source_paragraph', $source_paragraph);

      // Check if this bundle is allowed in the parent field definition.
      $details = $this->getParentFieldDetails($form_state);
      $target_bundles = [];

      if ($details['parent'] instanceof EntityInterface) {
        $parent = $details['parent'];
        $entity_type_id = $parent->getEntityTypeId();
        $parent_bundle = $details['parent']->getType();
        $node_fields = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $parent_bundle);

        if (isset($node_fields[$details['field_name']])) {
          $field = $node_fields[$details['field_name']];
          if ($field instanceof \Drupal\field\Entity\FieldConfig) {
            if ($field->getType() == 'entity_reference_revisions') {
              $target_bundles = $field->getSettings()['handler_settings']['target_bundles'];
            }
          }
        }
      }

      if (!in_array($source_paragraph_type, $target_bundles)) {
        $form_state->setError($form['paragraph_uuid'], $this->t(':bundle is not allowed by :field_name', [
          ':bundle' => $source_paragraph_type,
          ':field_name' => $details['field_name'],
        ]));
      }
    }
    catch (EntityStorageException | LogicException $e) {
      $form_state->setError($form['paragraph_uuid'], $e->getMessage());
      $form_state->setTemporaryValue('errors', $form_state->getErrors());
    }
  }

  /**
   * Form submit handler.
   *
   * @param array $form
   *   The form array
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function pasteSubmit(array $form, FormStateInterface $form_state) {
    $form_storage = $form_state->getStorage();
    /** @var Paragraph $source_paragraph */
    $source_paragraph = $form_state->getTemporaryValue('source_paragraph');
    if (!$source_paragraph) {
      return $form;
    }

    $details = $this->getParentFieldDetails($form_state);
    $new_paragraph = $source_paragraph->createDuplicate();
    $new_paragraph->setParentEntity($details['parent'], $details['field_name']);
    $form_storage['field_storage']['#parents']['#fields'][$details['field_name']]['items'][$details['delta']]['entity'] = $new_paragraph;
    $form_state->setStorage($form_storage);
    $form_state->setRebuild();
  }

  /**
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state to process.
   *
   * @return array
   *   An array containing the field_name, parent, and delta of trigger.
   */
  private function getParentFieldDetails(FormStateInterface $form_state): array
  {
    $trigger = $form_state->getTriggeringElement();
    $element_parents = $trigger['#parents'];
    // Work through the button parents to get critical information about the
    // paragraph being modified and where it sits in the current form.
    // Remove the "Rebuild" button.
    array_pop($element_parents);
    // Remove the "paste" wrapper.
    array_pop($element_parents);
    // Remove the "behavior_plugins" wrapper.
    array_pop($element_parents);
    // Remove the delta key.
    $paragraph_delta = array_pop($element_parents);
    // Remove "entity_form" wrapper.
    array_pop($element_parents);
    // Remove the field name.
    $field_name = array_pop($element_parents);

    return [
      'field_name' => $field_name,
      'parent' => $trigger['#parent_entity'],
      'delta' => $paragraph_delta,
    ];
  }

  /**
   * Ajax callback to refresh the Layout Paragraph widget from after paste.
   *
   * @param array $form
   *   The form array
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function pasteAjax(array $form, FormStateInterface $form_state) {
    $clicked_button = $form_state->getTriggeringElement();
    $element_parents = $clicked_button['#parents'];
    $field_name = $element_parents[0];

    $widget_parents = [$field_name, 'widget', 'entity_form'];
    $widget_form = NestedArray::getValue($form, $widget_parents);

    // @codingStandardsIgnoreStart
    $response = new AjaxResponse();
    if ($form_state->hasAnyErrors()) {
      $status_messages = [
        'status' => [
          '#weight' => -100,
          '#type' => 'status_messages',
        ],
      ];
      $response->addCommand(new PrependCommand('.layout-paragraphs-form', $status_messages));
    }
    $response->addCommand(new ReplaceCommand('.layout-paragraphs-form', $widget_form));
    // @codingStandardsIgnoreStop
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode) {

  }

}
