/**
 * @file
 * Provides Clipboard copy functionality.
 */
((Drupal, once) => {
  /**
   * Process elements with the .js-ditto-copy-uuid class on context load.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches dittoCopyUuid behaviors.
   */
  Drupal.behaviors.dittoCopyUuid = {
    attach(context) {
      once(
        'ditto-copy-uuid',
        '.js-ditto-copy-uuid',
        context
      ).forEach((element) => {
        element.addEventListener('click', (e) => {
          const { uuid } = element.dataset;
          navigator.clipboard.writeText(uuid).then(function() {
            console.log('Ditto: Copying to clipboard was successful!');
            element.classList.add('copied');
          }, function(err) {
            console.error('Ditto: Could not copy text: ', err);
          });
          element.classList.add('copied');
          setTimeout(function() {
            element.classList.remove('copied');
          }, 1000); // Remove the class after 1 second.
          e.preventDefault();
        }, true);
      });
    },
  };

  Drupal.behaviors.dittoPasteUuid = {
    attach(context) {
      once(
        'ditto-paste-uuid',
        '.js-ditto-paste-uuid',
        context
      ).forEach((element) => {
        let formWrapper = element.closest('.js-form-wrapper');
        let pasteSubmitButton = formWrapper.querySelector('input[type="submit"]');
        if (pasteSubmitButton) {
          pasteSubmitButton.style.visibility = 'hidden';
          element.addEventListener('paste', (event) => {
            pasteSubmitButton.click();
          });
          pasteSubmitButton.addEventListener('click', (event) => {
            event.stopPropagation();
          }, {
            capture: true,
          });
        }
      });
    }
  }
})(Drupal, once);
